import React from 'react';
import MovieList from './MovieList';
import MovieService from '../../services/MovieService';
import './style.css';

const ParentMovieComponent = () => (
  <div className="container">
    <div className="movie_Height">
      <MovieList movies={MovieService.getMovies()} />
    </div>
  </div>
);
export default ParentMovieComponent;
