import React from 'react';
import PropTypes from 'prop-types';
import MovieCard from '../MovieCard';

const getMovies = movies => {
  return (
    <div className="card-deck">
      <div className="row">
        {movies.map(movie => (
          <div className="col-md-4" key={movie.id}>
            <MovieCard movie={movie} />
          </div>
        ))}
      </div>
    </div>
  );
};

const MovieList = props => <div>{getMovies(props.movies)}</div>;

MovieList.defaultProps = {
  movies: [],
};

MovieList.propTypes = {
  movies: PropTypes.array,
};

export default MovieList;
