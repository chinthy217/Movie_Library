import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const MovieCard = props => {
  console.log(props);
  return (
    <div className="card mb-4 box-shadow">
      <img
        className="card-img-top"
        src={props.movie.imageUrl}
        alt="Card Image Top"
      />
      <div className="card-body">
        <p className="card-text">{props.movie.description}</p>
        <div className="d-flex justify-content-between align-items-center">
          <div className="btn-group">
            <button type="button" className="btn btn-sm btn-outline-secondary">
              View
            </button>
            <button type="button" className="btn btn-sm btn-outline-secondary">
              Edit
            </button>
          </div>
          <small className="text-muted">9 mins</small>
        </div>
      </div>
    </div>
  );
};

MovieCard.defaultProps = {
  movie: {},
};

MovieCard.propTypes = {
  movie: PropTypes.object,
};

export default MovieCard;
