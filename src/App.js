import React, { Component } from 'react';

import Header from './Components/Header';
import Movies from './Components/ParentMovieComponent';

class App extends Component {
  render() {
    return (
      <div>
        <Header title="Welcome To Movie World" />
        <Movies />
      </div>
    );
  }
}

export default App;
